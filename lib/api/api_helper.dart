import 'package:http/http.dart' as http;
import 'package:swasthyasetu/utils/multipart_request_with_progress.dart';

class ApiHelper {
  callApiWithHeadersAndBody(
      {String url, Map<String, String> headers, dynamic body}) async {
    /*headers['a'] = "aa";
    debugPrint("Let's print - ${headers}");*/
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );
    return response;
  }

  Future<http.StreamedResponse> callMultipartApi(
      MultipartRequest multipartRequest) async {
    /*multipartRequest.headers['a'] = "aa";
    debugPrint("Let's print - ${multipartRequest.headers}");*/
    var response = await multipartRequest.send();
    return response;
  }
}
