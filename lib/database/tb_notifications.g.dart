// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tb_notifications.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class TbNotificationTable extends DataClass implements Insertable<TbNotificationTable> {
  final int notificationID;
  final int reminderID;
  final DateTime shootTime;
  final DateTime entryTime;

  TbNotificationTable({this.notificationID, this.reminderID, this.shootTime, this.entryTime});

  factory TbNotificationTable.fromData(Map<String, dynamic> data, GeneratedDatabase db, {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return TbNotificationTable(
      notificationID: intType.mapFromDatabaseResponse(data['${effectivePrefix}notification_i_d']),
      reminderID: intType.mapFromDatabaseResponse(data['${effectivePrefix}reminder_i_d']),
      shootTime: dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}shoot_time']),
      entryTime: dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}entry_time']),
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || notificationID != null) {
      map['notification_i_d'] = Variable<int>(notificationID);
    }
    if (!nullToAbsent || reminderID != null) {
      map['reminder_i_d'] = Variable<int>(reminderID);
    }
    if (!nullToAbsent || shootTime != null) {
      map['shoot_time'] = Variable<DateTime>(shootTime);
    }
    if (!nullToAbsent || entryTime != null) {
      map['entry_time'] = Variable<DateTime>(entryTime);
    }
    return map;
  }

  TbNotificationCompanion toCompanion(bool nullToAbsent) {
    return TbNotificationCompanion(
      notificationID: notificationID == null && nullToAbsent ? const Value.absent() : Value(notificationID),
      reminderID: reminderID == null && nullToAbsent ? const Value.absent() : Value(reminderID),
      shootTime: shootTime == null && nullToAbsent ? const Value.absent() : Value(shootTime),
      entryTime: entryTime == null && nullToAbsent ? const Value.absent() : Value(entryTime),
    );
  }

  factory TbNotificationTable.fromJson(Map<String, dynamic> json, {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return TbNotificationTable(
      notificationID: serializer.fromJson<int>(json['notificationID']),
      reminderID: serializer.fromJson<int>(json['reminderID']),
      shootTime: serializer.fromJson<DateTime>(json['shootTime']),
      entryTime: serializer.fromJson<DateTime>(json['entryTime']),
    );
  }

  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'notificationID': serializer.toJson<int>(notificationID),
      'reminderID': serializer.toJson<int>(reminderID),
      'shootTime': serializer.toJson<DateTime>(shootTime),
      'entryTime': serializer.toJson<DateTime>(entryTime),
    };
  }

  TbNotificationTable copyWith({int notificationID, int reminderID, DateTime shootTime, DateTime entryTime}) =>
      TbNotificationTable(
        notificationID: notificationID ?? this.notificationID,
        reminderID: reminderID ?? this.reminderID,
        shootTime: shootTime ?? this.shootTime,
        entryTime: entryTime ?? this.entryTime,
      );

  @override
  String toString() {
    return (StringBuffer('TbNotificationTable(')
          ..write('notificationID: $notificationID, ')
          ..write('reminderID: $reminderID, ')
          ..write('shootTime: $shootTime, ')
          ..write('entryTime: $entryTime')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(notificationID.hashCode, $mrjc(reminderID.hashCode, $mrjc(shootTime.hashCode, entryTime.hashCode))));

  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is TbNotificationTable &&
          other.notificationID == this.notificationID &&
          other.reminderID == this.reminderID &&
          other.shootTime == this.shootTime &&
          other.entryTime == this.entryTime);
}

class TbNotificationCompanion extends UpdateCompanion<TbNotificationTable> {
  final Value<int> notificationID;
  final Value<int> reminderID;
  final Value<DateTime> shootTime;
  final Value<DateTime> entryTime;

  const TbNotificationCompanion({
    this.notificationID = const Value.absent(),
    this.reminderID = const Value.absent(),
    this.shootTime = const Value.absent(),
    this.entryTime = const Value.absent(),
  });

  TbNotificationCompanion.insert({
    this.notificationID = const Value.absent(),
    @required int reminderID,
    this.shootTime = const Value.absent(),
    this.entryTime = const Value.absent(),
  }) : reminderID = Value(reminderID);

  static Insertable<TbNotificationTable> custom({
    Expression<int> notificationID,
    Expression<int> reminderID,
    Expression<DateTime> shootTime,
    Expression<DateTime> entryTime,
  }) {
    return RawValuesInsertable({
      if (notificationID != null) 'notification_i_d': notificationID,
      if (reminderID != null) 'reminder_i_d': reminderID,
      if (shootTime != null) 'shoot_time': shootTime,
      if (entryTime != null) 'entry_time': entryTime,
    });
  }

  TbNotificationCompanion copyWith(
      {Value<int> notificationID, Value<int> reminderID, Value<DateTime> shootTime, Value<DateTime> entryTime}) {
    return TbNotificationCompanion(
      notificationID: notificationID ?? this.notificationID,
      reminderID: reminderID ?? this.reminderID,
      shootTime: shootTime ?? this.shootTime,
      entryTime: entryTime ?? this.entryTime,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (notificationID.present) {
      map['notification_i_d'] = Variable<int>(notificationID.value);
    }
    if (reminderID.present) {
      map['reminder_i_d'] = Variable<int>(reminderID.value);
    }
    if (shootTime.present) {
      map['shoot_time'] = Variable<DateTime>(shootTime.value);
    }
    if (entryTime.present) {
      map['entry_time'] = Variable<DateTime>(entryTime.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbNotificationCompanion(')
          ..write('notificationID: $notificationID, ')
          ..write('reminderID: $reminderID, ')
          ..write('shootTime: $shootTime, ')
          ..write('entryTime: $entryTime')
          ..write(')'))
        .toString();
  }
}

class $TbNotificationTable extends TbNotification with TableInfo<$TbNotificationTable, TbNotificationTable> {
  final GeneratedDatabase _db;
  final String _alias;

  $TbNotificationTable(this._db, [this._alias]);

  final VerificationMeta _notificationIDMeta = const VerificationMeta('notificationID');
  GeneratedColumn<int> _notificationID;

  @override
  GeneratedColumn<int> get notificationID => _notificationID ??= _constructNotificationID();

  // GeneratedColumn<int> _constructNotificationID() {
  //   return GeneratedIntColumn('notificationID', $tableName, false,
  //       hasAutoIncrement: true, declaredAsPrimaryKey: true);
  // }

  GeneratedColumn<int> _constructNotificationID() {
    return GeneratedColumn<int>(
      'notificationID',
      $tableName,
      false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT',
    );
  }

  final VerificationMeta _reminderIDMeta = const VerificationMeta('reminderID');
  GeneratedColumn<int> _reminderID;

  @override
  GeneratedColumn<int> get reminderID => _reminderID ??= _constructReminderID();

  GeneratedColumn<int> _constructReminderID() {
    return GeneratedColumn<int>(
      'reminderID',
      $tableName,
      false,
    );
  }

  final VerificationMeta _shootTimeMeta = const VerificationMeta('shootTime');
  GeneratedColumn<DateTime> _shootTime;

  @override
  GeneratedColumn<DateTime> get shootTime => _shootTime ??= _constructShootTime();

  GeneratedColumn<DateTime> _constructShootTime() {
    return GeneratedColumn<DateTime>(
      'shoot_time',
      $tableName,
      true,
    );
  }

  final VerificationMeta _entryTimeMeta = const VerificationMeta('entryTime');
  GeneratedColumn<DateTime> _entryTime;

  @override
  GeneratedColumn<DateTime> get entryTime => _entryTime ??= _constructEntryTime();

  GeneratedColumn<DateTime> _constructEntryTime() {
    return GeneratedColumn<DateTime>(
      'entry_time',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [notificationID, reminderID, shootTime, entryTime];

  @override
  $TbNotificationTable get asDslTable => this;

  @override
  String get $tableName => _alias ?? 'tb_notification';
  @override
  final String actualTableName = 'tb_notification';

  @override
  VerificationContext validateIntegrity(Insertable<TbNotificationTable> instance, {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('notification_i_d')) {
      context.handle(
          _notificationIDMeta, notificationID.isAcceptableOrUnknown(data['notification_i_d'], _notificationIDMeta));
    }
    if (data.containsKey('reminder_i_d')) {
      context.handle(_reminderIDMeta, reminderID.isAcceptableOrUnknown(data['reminder_i_d'], _reminderIDMeta));
    } else if (isInserting) {
      context.missing(_reminderIDMeta);
    }
    if (data.containsKey('shoot_time')) {
      context.handle(_shootTimeMeta, shootTime.isAcceptableOrUnknown(data['shoot_time'], _shootTimeMeta));
    }
    if (data.containsKey('entry_time')) {
      context.handle(_entryTimeMeta, entryTime.isAcceptableOrUnknown(data['entry_time'], _entryTimeMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {notificationID};

  @override
  TbNotificationTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return TbNotificationTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $TbNotificationTable createAlias(String alias) {
    return $TbNotificationTable(_db, alias);
  }

  @override
  // TODO: implement attachedDatabase
  DatabaseConnectionUser get attachedDatabase => throw UnimplementedError();
}

abstract class _$AppDatabaseNotification extends GeneratedDatabase {
  _$AppDatabaseNotification(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $TbNotificationTable _tbNotification;

  $TbNotificationTable get tbNotification => _tbNotification ??= $TbNotificationTable(this);

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();

  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [tbNotification];
}
