// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tb_reminder.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class TbReminderTable extends DataClass implements Insertable<TbReminderTable> {
  final int reminderID;
  final int notificationID;
  final String category;
  final String desc;
  final bool isReminderOn;
  final bool isOnceOrFrequent;
  final DateTime onceTime;
  final DateTime fromTime;
  final DateTime toTime;
  final DateTime entryTime;
  final int frequentlyEveryHours;
  TbReminderTable(
      {@required this.reminderID,
      @required this.notificationID,
      @required this.category,
      this.desc,
      @required this.isReminderOn,
      @required this.isOnceOrFrequent,
      this.onceTime,
      this.fromTime,
      this.toTime,
      this.entryTime,
      this.frequentlyEveryHours});
  factory TbReminderTable.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return TbReminderTable(
      reminderID: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}reminder_i_d']),
      notificationID: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}notification_i_d']),
      category: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}category']),
      desc: stringType.mapFromDatabaseResponse(data['${effectivePrefix}desc']),
      isReminderOn: boolType
          .mapFromDatabaseResponse(data['${effectivePrefix}is_reminder_on']),
      isOnceOrFrequent: boolType.mapFromDatabaseResponse(
          data['${effectivePrefix}is_once_or_frequent']),
      onceTime: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}once_time']),
      fromTime: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}from_time']),
      toTime: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}to_time']),
      entryTime: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}entry_time']),
      frequentlyEveryHours: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}frequently_every_hours']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || reminderID != null) {
      map['reminder_i_d'] = Variable<int>(reminderID);
    }
    if (!nullToAbsent || notificationID != null) {
      map['notification_i_d'] = Variable<int>(notificationID);
    }
    if (!nullToAbsent || category != null) {
      map['category'] = Variable<String>(category);
    }
    if (!nullToAbsent || desc != null) {
      map['desc'] = Variable<String>(desc);
    }
    if (!nullToAbsent || isReminderOn != null) {
      map['is_reminder_on'] = Variable<bool>(isReminderOn);
    }
    if (!nullToAbsent || isOnceOrFrequent != null) {
      map['is_once_or_frequent'] = Variable<bool>(isOnceOrFrequent);
    }
    if (!nullToAbsent || onceTime != null) {
      map['once_time'] = Variable<DateTime>(onceTime);
    }
    if (!nullToAbsent || fromTime != null) {
      map['from_time'] = Variable<DateTime>(fromTime);
    }
    if (!nullToAbsent || toTime != null) {
      map['to_time'] = Variable<DateTime>(toTime);
    }
    if (!nullToAbsent || entryTime != null) {
      map['entry_time'] = Variable<DateTime>(entryTime);
    }
    if (!nullToAbsent || frequentlyEveryHours != null) {
      map['frequently_every_hours'] = Variable<int>(frequentlyEveryHours);
    }
    return map;
  }

  TbReminderCompanion toCompanion(bool nullToAbsent) {
    return TbReminderCompanion(
      reminderID: reminderID == null && nullToAbsent
          ? const Value.absent()
          : Value(reminderID),
      notificationID: notificationID == null && nullToAbsent
          ? const Value.absent()
          : Value(notificationID),
      category: category == null && nullToAbsent
          ? const Value.absent()
          : Value(category),
      desc: desc == null && nullToAbsent ? const Value.absent() : Value(desc),
      isReminderOn: isReminderOn == null && nullToAbsent
          ? const Value.absent()
          : Value(isReminderOn),
      isOnceOrFrequent: isOnceOrFrequent == null && nullToAbsent
          ? const Value.absent()
          : Value(isOnceOrFrequent),
      onceTime: onceTime == null && nullToAbsent
          ? const Value.absent()
          : Value(onceTime),
      fromTime: fromTime == null && nullToAbsent
          ? const Value.absent()
          : Value(fromTime),
      toTime:
          toTime == null && nullToAbsent ? const Value.absent() : Value(toTime),
      entryTime: entryTime == null && nullToAbsent
          ? const Value.absent()
          : Value(entryTime),
      frequentlyEveryHours: frequentlyEveryHours == null && nullToAbsent
          ? const Value.absent()
          : Value(frequentlyEveryHours),
    );
  }

  factory TbReminderTable.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return TbReminderTable(
      reminderID: serializer.fromJson<int>(json['reminderID']),
      notificationID: serializer.fromJson<int>(json['notificationID']),
      category: serializer.fromJson<String>(json['category']),
      desc: serializer.fromJson<String>(json['desc']),
      isReminderOn: serializer.fromJson<bool>(json['isReminderOn']),
      isOnceOrFrequent: serializer.fromJson<bool>(json['isOnceOrFrequent']),
      onceTime: serializer.fromJson<DateTime>(json['onceTime']),
      fromTime: serializer.fromJson<DateTime>(json['fromTime']),
      toTime: serializer.fromJson<DateTime>(json['toTime']),
      entryTime: serializer.fromJson<DateTime>(json['entryTime']),
      frequentlyEveryHours:
          serializer.fromJson<int>(json['frequentlyEveryHours']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'reminderID': serializer.toJson<int>(reminderID),
      'notificationID': serializer.toJson<int>(notificationID),
      'category': serializer.toJson<String>(category),
      'desc': serializer.toJson<String>(desc),
      'isReminderOn': serializer.toJson<bool>(isReminderOn),
      'isOnceOrFrequent': serializer.toJson<bool>(isOnceOrFrequent),
      'onceTime': serializer.toJson<DateTime>(onceTime),
      'fromTime': serializer.toJson<DateTime>(fromTime),
      'toTime': serializer.toJson<DateTime>(toTime),
      'entryTime': serializer.toJson<DateTime>(entryTime),
      'frequentlyEveryHours': serializer.toJson<int>(frequentlyEveryHours),
    };
  }

  TbReminderTable copyWith(
          {int reminderID,
          int notificationID,
          String category,
          String desc,
          bool isReminderOn,
          bool isOnceOrFrequent,
          DateTime onceTime,
          DateTime fromTime,
          DateTime toTime,
          DateTime entryTime,
          int frequentlyEveryHours}) =>
      TbReminderTable(
        reminderID: reminderID ?? this.reminderID,
        notificationID: notificationID ?? this.notificationID,
        category: category ?? this.category,
        desc: desc ?? this.desc,
        isReminderOn: isReminderOn ?? this.isReminderOn,
        isOnceOrFrequent: isOnceOrFrequent ?? this.isOnceOrFrequent,
        onceTime: onceTime ?? this.onceTime,
        fromTime: fromTime ?? this.fromTime,
        toTime: toTime ?? this.toTime,
        entryTime: entryTime ?? this.entryTime,
        frequentlyEveryHours: frequentlyEveryHours ?? this.frequentlyEveryHours,
      );
  @override
  String toString() {
    return (StringBuffer('TbReminderTable(')
          ..write('reminderID: $reminderID, ')
          ..write('notificationID: $notificationID, ')
          ..write('category: $category, ')
          ..write('desc: $desc, ')
          ..write('isReminderOn: $isReminderOn, ')
          ..write('isOnceOrFrequent: $isOnceOrFrequent, ')
          ..write('onceTime: $onceTime, ')
          ..write('fromTime: $fromTime, ')
          ..write('toTime: $toTime, ')
          ..write('entryTime: $entryTime, ')
          ..write('frequentlyEveryHours: $frequentlyEveryHours')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      reminderID.hashCode,
      $mrjc(
          notificationID.hashCode,
          $mrjc(
              category.hashCode,
              $mrjc(
                  desc.hashCode,
                  $mrjc(
                      isReminderOn.hashCode,
                      $mrjc(
                          isOnceOrFrequent.hashCode,
                          $mrjc(
                              onceTime.hashCode,
                              $mrjc(
                                  fromTime.hashCode,
                                  $mrjc(
                                      toTime.hashCode,
                                      $mrjc(
                                          entryTime.hashCode,
                                          frequentlyEveryHours
                                              .hashCode)))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is TbReminderTable &&
          other.reminderID == this.reminderID &&
          other.notificationID == this.notificationID &&
          other.category == this.category &&
          other.desc == this.desc &&
          other.isReminderOn == this.isReminderOn &&
          other.isOnceOrFrequent == this.isOnceOrFrequent &&
          other.onceTime == this.onceTime &&
          other.fromTime == this.fromTime &&
          other.toTime == this.toTime &&
          other.entryTime == this.entryTime &&
          other.frequentlyEveryHours == this.frequentlyEveryHours);
}

class TbReminderCompanion extends UpdateCompanion<TbReminderTable> {
  final Value<int> reminderID;
  final Value<int> notificationID;
  final Value<String> category;
  final Value<String> desc;
  final Value<bool> isReminderOn;
  final Value<bool> isOnceOrFrequent;
  final Value<DateTime> onceTime;
  final Value<DateTime> fromTime;
  final Value<DateTime> toTime;
  final Value<DateTime> entryTime;
  final Value<int> frequentlyEveryHours;
  const TbReminderCompanion({
    this.reminderID = const Value.absent(),
    this.notificationID = const Value.absent(),
    this.category = const Value.absent(),
    this.desc = const Value.absent(),
    this.isReminderOn = const Value.absent(),
    this.isOnceOrFrequent = const Value.absent(),
    this.onceTime = const Value.absent(),
    this.fromTime = const Value.absent(),
    this.toTime = const Value.absent(),
    this.entryTime = const Value.absent(),
    this.frequentlyEveryHours = const Value.absent(),
  });
  TbReminderCompanion.insert({
    this.reminderID = const Value.absent(),
    @required int notificationID,
    @required String category,
    this.desc = const Value.absent(),
    this.isReminderOn = const Value.absent(),
    this.isOnceOrFrequent = const Value.absent(),
    this.onceTime = const Value.absent(),
    this.fromTime = const Value.absent(),
    this.toTime = const Value.absent(),
    this.entryTime = const Value.absent(),
    this.frequentlyEveryHours = const Value.absent(),
  })  : notificationID = Value(notificationID),
        category = Value(category);
  static Insertable<TbReminderTable> custom({
    Expression<int> reminderID,
    Expression<int> notificationID,
    Expression<String> category,
    Expression<String> desc,
    Expression<bool> isReminderOn,
    Expression<bool> isOnceOrFrequent,
    Expression<DateTime> onceTime,
    Expression<DateTime> fromTime,
    Expression<DateTime> toTime,
    Expression<DateTime> entryTime,
    Expression<int> frequentlyEveryHours,
  }) {
    return RawValuesInsertable({
      if (reminderID != null) 'reminder_i_d': reminderID,
      if (notificationID != null) 'notification_i_d': notificationID,
      if (category != null) 'category': category,
      if (desc != null) 'desc': desc,
      if (isReminderOn != null) 'is_reminder_on': isReminderOn,
      if (isOnceOrFrequent != null) 'is_once_or_frequent': isOnceOrFrequent,
      if (onceTime != null) 'once_time': onceTime,
      if (fromTime != null) 'from_time': fromTime,
      if (toTime != null) 'to_time': toTime,
      if (entryTime != null) 'entry_time': entryTime,
      if (frequentlyEveryHours != null)
        'frequently_every_hours': frequentlyEveryHours,
    });
  }

  TbReminderCompanion copyWith(
      {Value<int> reminderID,
      Value<int> notificationID,
      Value<String> category,
      Value<String> desc,
      Value<bool> isReminderOn,
      Value<bool> isOnceOrFrequent,
      Value<DateTime> onceTime,
      Value<DateTime> fromTime,
      Value<DateTime> toTime,
      Value<DateTime> entryTime,
      Value<int> frequentlyEveryHours}) {
    return TbReminderCompanion(
      reminderID: reminderID ?? this.reminderID,
      notificationID: notificationID ?? this.notificationID,
      category: category ?? this.category,
      desc: desc ?? this.desc,
      isReminderOn: isReminderOn ?? this.isReminderOn,
      isOnceOrFrequent: isOnceOrFrequent ?? this.isOnceOrFrequent,
      onceTime: onceTime ?? this.onceTime,
      fromTime: fromTime ?? this.fromTime,
      toTime: toTime ?? this.toTime,
      entryTime: entryTime ?? this.entryTime,
      frequentlyEveryHours: frequentlyEveryHours ?? this.frequentlyEveryHours,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (reminderID.present) {
      map['reminder_i_d'] = Variable<int>(reminderID.value);
    }
    if (notificationID.present) {
      map['notification_i_d'] = Variable<int>(notificationID.value);
    }
    if (category.present) {
      map['category'] = Variable<String>(category.value);
    }
    if (desc.present) {
      map['desc'] = Variable<String>(desc.value);
    }
    if (isReminderOn.present) {
      map['is_reminder_on'] = Variable<bool>(isReminderOn.value);
    }
    if (isOnceOrFrequent.present) {
      map['is_once_or_frequent'] = Variable<bool>(isOnceOrFrequent.value);
    }
    if (onceTime.present) {
      map['once_time'] = Variable<DateTime>(onceTime.value);
    }
    if (fromTime.present) {
      map['from_time'] = Variable<DateTime>(fromTime.value);
    }
    if (toTime.present) {
      map['to_time'] = Variable<DateTime>(toTime.value);
    }
    if (entryTime.present) {
      map['entry_time'] = Variable<DateTime>(entryTime.value);
    }
    if (frequentlyEveryHours.present) {
      map['frequently_every_hours'] = Variable<int>(frequentlyEveryHours.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TbReminderCompanion(')
          ..write('reminderID: $reminderID, ')
          ..write('notificationID: $notificationID, ')
          ..write('category: $category, ')
          ..write('desc: $desc, ')
          ..write('isReminderOn: $isReminderOn, ')
          ..write('isOnceOrFrequent: $isOnceOrFrequent, ')
          ..write('onceTime: $onceTime, ')
          ..write('fromTime: $fromTime, ')
          ..write('toTime: $toTime, ')
          ..write('entryTime: $entryTime, ')
          ..write('frequentlyEveryHours: $frequentlyEveryHours')
          ..write(')'))
        .toString();
  }
}

class $TbReminderTable extends TbReminder
    with TableInfo<$TbReminderTable, TbReminderTable> {
  final GeneratedDatabase _db;
  final String _alias;
  $TbReminderTable(this._db, [this._alias]);
  final VerificationMeta _reminderIDMeta = const VerificationMeta('reminderID');
  GeneratedColumn<int> _reminderID;
  @override
  GeneratedColumn<int> get reminderID => _reminderID ??= _constructReminderID();
  // GeneratedColumn<int> _constructReminderID() {
  //   return GeneratedIntColumn('reminder_i_d', $tableName, false,
  //       hasAutoIncrement: true, declaredAsPrimaryKey: true);
  // }

  GeneratedColumn<int> _constructReminderID() {
    return GeneratedColumn<int>(
      'reminderID',
      $tableName,
      false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT',
    );
  }

  final VerificationMeta _notificationIDMeta =
      const VerificationMeta('notificationID');
  GeneratedColumn<int> _notificationID;
  @override
  GeneratedColumn<int> get notificationID =>
      _notificationID ??= _constructNotificationID();
  GeneratedColumn<int> _constructNotificationID() {
    return GeneratedColumn<int>(
      'notificationID',
      $tableName,
      false,
    );
  }

  final VerificationMeta _categoryMeta = const VerificationMeta('category');
  GeneratedColumn<String> _category;

  @override
  GeneratedColumn<String> get category => _category ??= _constructCategory();
  // GeneratedColumn<String> _constructCategory() {
  //   return GeneratedColumn<String>('category', $tableName, false,
  //       minTextLength: 1, maxTextLength: 50);
  // }

  GeneratedColumn<String> _constructCategory() {
    return GeneratedColumn<String>(
      'category',
      $tableName,
      false,
    );
  }

  final VerificationMeta _descMeta = const VerificationMeta('desc');
  GeneratedColumn<String> _desc;
  @override
  GeneratedColumn<String> get desc => _desc ??= _constructDesc();
  // GeneratedColumn<String> _constructDesc() {
  //   return GeneratedColumn<String>('desc', $tableName, true,
  //       minTextLength: 0, maxTextLength: 2000);
  // }

  GeneratedColumn<String> _constructDesc() {
    return GeneratedColumn<String>(
      'desc',
      $tableName,
      false,
    );
  }

  final VerificationMeta _isReminderOnMeta =
      const VerificationMeta('isReminderOn');
  GeneratedColumn<bool> _isReminderOn;
  @override
  GeneratedColumn<bool> get isReminderOn =>
      _isReminderOn ??= _constructIsReminderOn();
  GeneratedColumn<bool> _constructIsReminderOn() {
    return GeneratedColumn<bool>('is_reminder_on', $tableName, false,
        defaultValue: Constant(false));
  }

  final VerificationMeta _isOnceOrFrequentMeta =
      const VerificationMeta('isOnceOrFrequent');
  GeneratedColumn<bool> _isOnceOrFrequent;

  @override
  GeneratedColumn<bool> get isOnceOrFrequent =>
      _isOnceOrFrequent ??= _constructIsOnceOrFrequent();
  GeneratedColumn<bool> _constructIsOnceOrFrequent() {
    return GeneratedColumn<bool>('isOnceOrFrequent', $tableName, false,
        defaultValue: Constant(false));
  }

  final VerificationMeta _onceTimeMeta = const VerificationMeta('onceTime');
  GeneratedColumn<DateTime> _onceTime;
  @override
  GeneratedColumn<DateTime> get onceTime => _onceTime ??= _constructOnceTime();
  GeneratedColumn<DateTime> _constructOnceTime() {
    return GeneratedColumn<DateTime>(
      'onceTime',
      $tableName,
      true,
    );
  }

  final VerificationMeta _fromTimeMeta = const VerificationMeta('fromTime');
  GeneratedColumn<DateTime> _fromTime;
  @override
  GeneratedColumn<DateTime> get fromTime => _fromTime ??= _constructFromTime();
  GeneratedColumn<DateTime> _constructFromTime() {
    return GeneratedColumn<DateTime>(
      'from_time',
      $tableName,
      true,
    );
  }

  final VerificationMeta _toTimeMeta = const VerificationMeta('toTime');
  GeneratedColumn<DateTime> _toTime;
  @override
  GeneratedColumn<DateTime> get toTime => _toTime ??= _constructToTime();
  GeneratedColumn<DateTime> _constructToTime() {
    return GeneratedColumn<DateTime>(
      'to_time',
      $tableName,
      true,
    );
  }

  final VerificationMeta _entryTimeMeta = const VerificationMeta('entryTime');
  GeneratedColumn<DateTime> _entryTime;
  @override
  GeneratedColumn<DateTime> get entryTime => _entryTime ??= _constructEntryTime();
  GeneratedColumn<DateTime> _constructEntryTime() {
    return GeneratedColumn<DateTime>(
      'entry_time',
      $tableName,
      true,
    );
  }

  final VerificationMeta _frequentlyEveryHoursMeta =
      const VerificationMeta('frequentlyEveryHours');
  GeneratedColumn<int> _frequentlyEveryHours;
  @override
  GeneratedColumn<int> get frequentlyEveryHours =>
      _frequentlyEveryHours ??= _constructFrequentlyEveryHours();
  GeneratedColumn<int> _constructFrequentlyEveryHours() {
    return GeneratedColumn<int>(
      'frequently_every_hours',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        reminderID,
        notificationID,
        category,
        desc,
        isReminderOn,
        isOnceOrFrequent,
        onceTime,
        fromTime,
        toTime,
        entryTime,
        frequentlyEveryHours
      ];
  @override
  $TbReminderTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'tb_reminder';
  @override
  final String actualTableName = 'tb_reminder';
  @override
  VerificationContext validateIntegrity(Insertable<TbReminderTable> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('reminder_i_d')) {
      context.handle(
          _reminderIDMeta,
          reminderID.isAcceptableOrUnknown(
              data['reminder_i_d'], _reminderIDMeta));
    }
    if (data.containsKey('notification_i_d')) {
      context.handle(
          _notificationIDMeta,
          notificationID.isAcceptableOrUnknown(
              data['notification_i_d'], _notificationIDMeta));
    } else if (isInserting) {
      context.missing(_notificationIDMeta);
    }
    if (data.containsKey('category')) {
      context.handle(_categoryMeta,
          category.isAcceptableOrUnknown(data['category'], _categoryMeta));
    } else if (isInserting) {
      context.missing(_categoryMeta);
    }
    if (data.containsKey('desc')) {
      context.handle(
          _descMeta, desc.isAcceptableOrUnknown(data['desc'], _descMeta));
    }
    if (data.containsKey('is_reminder_on')) {
      context.handle(
          _isReminderOnMeta,
          isReminderOn.isAcceptableOrUnknown(
              data['is_reminder_on'], _isReminderOnMeta));
    }
    if (data.containsKey('is_once_or_frequent')) {
      context.handle(
          _isOnceOrFrequentMeta,
          isOnceOrFrequent.isAcceptableOrUnknown(
              data['is_once_or_frequent'], _isOnceOrFrequentMeta));
    }
    if (data.containsKey('once_time')) {
      context.handle(_onceTimeMeta,
          onceTime.isAcceptableOrUnknown(data['once_time'], _onceTimeMeta));
    }
    if (data.containsKey('from_time')) {
      context.handle(_fromTimeMeta,
          fromTime.isAcceptableOrUnknown(data['from_time'], _fromTimeMeta));
    }
    if (data.containsKey('to_time')) {
      context.handle(_toTimeMeta,
          toTime.isAcceptableOrUnknown(data['to_time'], _toTimeMeta));
    }
    if (data.containsKey('entry_time')) {
      context.handle(_entryTimeMeta,
          entryTime.isAcceptableOrUnknown(data['entry_time'], _entryTimeMeta));
    }
    if (data.containsKey('frequently_every_hours')) {
      context.handle(
          _frequentlyEveryHoursMeta,
          frequentlyEveryHours.isAcceptableOrUnknown(
              data['frequently_every_hours'], _frequentlyEveryHoursMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {reminderID};
  @override
  TbReminderTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return TbReminderTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $TbReminderTable createAlias(String alias) {
    return $TbReminderTable(_db, alias);
  }

  @override
  // TODO: implement attachedDatabase
  DatabaseConnectionUser get attachedDatabase => throw UnimplementedError();
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $TbReminderTable _tbReminder;
  $TbReminderTable get tbReminder => _tbReminder ??= $TbReminderTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [tbReminder];
}
