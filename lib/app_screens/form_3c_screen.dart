import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:swasthyasetu/global/SizeConfig.dart';
import 'package:swasthyasetu/global/utils.dart';
import 'package:swasthyasetu/podo/response_main_model.dart';
import 'package:swasthyasetu/utils/progress_dialog.dart';
import 'package:swasthyasetu/widgets/date_range_picker_custom.dart'
    as DateRagePicker;

import '../utils/color.dart';

class Form3CScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Form3CScreenState();
  }
}

class Form3CScreenState extends State<Form3CScreen> {
  var fromDate = DateTime.now().subtract(Duration(days: 7));
  var toDate = DateTime.now();

  var fromDateString = "";
  var toDateString = "";
  var taskId;
  ProgressDialog pr;

  @override
  void initState() {
    super.initState();
    _bindBackgroundIsolate();
    FlutterDownloader.registerCallback(downloadCallback);
  }

  @override
  void dispose() {
    _unbindBackgroundIsolate();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Form 3C"),
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(color: colorBlack),
        textTheme: TextTheme(
          headline6: TextStyle(
            color: colorBlack,
            fontFamily: "Ubuntu",
            fontSize: SizeConfig.blockSizeVertical * 2.5,
          ),
        ),
      ),
      body: Builder(
        builder: (context) {
          return Column(
            children: [
              SizedBox(
                height: SizeConfig.blockSizeVertical * 2,
              ),
              Container(
                height: SizeConfig.blockSizeVertical * 8,
                child: Padding(
                  padding: EdgeInsets.only(left: 5.0, right: 5.0),
                  child: Container(
                    child: InkWell(
                        onTap: () {
                          showDateRangePickerDialog();
                        },
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                fromDateString == ""
                                    ? "Select Date Range"
                                    : "$fromDateString  to  $toDateString",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize:
                                        SizeConfig.blockSizeVertical * 2.6,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                              width: SizeConfig.blockSizeHorizontal * 15,
                              child: Icon(
                                Icons.arrow_drop_down,
                                size: SizeConfig.blockSizeHorizontal * 8,
                              ),
                            ),
                          ],
                        )),
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.blockSizeVertical * 2,
              ),
              MaterialButton(
                onPressed: () {
                  if (fromDateString == "") {
                    final snackBar = SnackBar(
                      backgroundColor: Colors.red,
                      content: Text("Please select Date Range"),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    return;
                  }
                  getPdfDownloadPath(context);
                },
                color: Colors.blue,
                child: Text(
                  "Submit",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: SizeConfig.blockSizeHorizontal * 4.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  Future<void> showDateRangePickerDialog() async {
    final List<DateTime> listPicked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: fromDate,
        initialLastDate: toDate,
        firstDate: DateTime.now().subtract(Duration(days: 365 * 100)),
        lastDate: DateTime.now(),
        handleOk: () {},
        handleCancel: () {});
    if (listPicked != null && listPicked.length == 2) {
      fromDate = listPicked[0];
      toDate = listPicked[1];
      var formatter = new DateFormat('dd-MM-yyyy');
      fromDateString = formatter.format(fromDate);
      toDateString = formatter.format(toDate);
      setState(() {});
    }
  }

  void showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
    }
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port');
    send.send([id, status, progress]);
  }

  void _bindBackgroundIsolate() {
    ReceivePort _port = ReceivePort();
    bool isSuccess = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      if (/*status == DownloadTaskStatus.complete*/ status.toString() ==
              "DownloadTaskStatus(3)" &&
          progress == 100 &&
          id != null) {
        debugPrint("Successfully downloaded");
        pr.hide();
        String query = "SELECT * FROM task WHERE task_id='" + id + "'";
        var tasks = FlutterDownloader.loadTasksWithRawQuery(query: query);
        if (tasks != null) FlutterDownloader.open(taskId: id);
      }
    });
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  void getPdfDownloadPath(BuildContext context) async {
    String loginUrl = "${baseURL}formpdfdoc.php";

    pr = ProgressDialog(context);
    pr.show();
    String doctorIDP = await getPatientOrDoctorIDP();
    String patientUniqueKey = await getPatientUniqueKey();
    String userType = await getUserType();
    debugPrint("Key and type");
    debugPrint(patientUniqueKey);
    debugPrint(userType);
    String jsonStr = "{" +
        "\"" +
        "DoctorIDP" +
        "\"" +
        ":" +
        "\"" +
        doctorIDP +
        "\"," +
        "\"" +
        "fromdate" +
        "\"" +
        ":" +
        "\"" +
        fromDateString +
        "\"" +
        ",\"todate\":\"$toDateString\"" +
        "}";

    debugPrint(jsonStr);

    String encodedJSONStr = encodeBase64(jsonStr);
    var response = await apiHelper.callApiWithHeadersAndBody(
      url: loginUrl,
      headers: {
        "u": patientUniqueKey,
        "type": userType,
      },
      body: {"getjson": encodedJSONStr},
    );
    debugPrint(response.body.toString());
    final jsonResponse = json.decode(response.body.toString());
    ResponseModel model = ResponseModel.fromJSON(jsonResponse);
    pr.hide();
    if (model.status == "OK") {
      String encodedFileName = model.data;
      String strData = decodeBase64(encodedFileName);
      final jsonData = json.decode(strData);
      String fileName = jsonData[0]['FileName'].toString();
      String downloadPdfUrl = "${baseURL}images/form3cdoc/$fileName";
      downloadAndOpenTheFile(downloadPdfUrl, fileName);
    } else {
      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text(model.message),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      pr.hide();
    }
  }

  void downloadAndOpenTheFile(String url, String fileName) async {
    var tempDir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    //await tempDir.create(recursive: true);
    String fullPath = tempDir.path + "/$fileName";
    debugPrint("full path");
    debugPrint(fullPath);
    Dio dio = Dio();
    downloadFileAndOpenActually(dio, url, fullPath);
  }

  Future downloadFileAndOpenActually(
      Dio dio, String url, String savePath) async {
    try {
      pr = ProgressDialog(context);
      pr.show();

      final savedDir = Directory(savePath);
      bool hasExisted = await savedDir.exists();
      if (!hasExisted) {
        await savedDir.create();
      }
      taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: savePath,
        showNotification: false,
        // show download progress in status bar (for Android)
        openFileFromNotification:
            false, // click on notification to open downloaded file (for Android)
      ) /*.then((value) {
        taskId = value;
      })*/
          ;
      var tasks = await FlutterDownloader.loadTasks();
      debugPrint("File path");
    } catch (e) {
      print("Error downloading");
      print(e.toString());
    }
  }

  String encodeBase64(String text) {
    var bytes = utf8.encode(text);
    return base64.encode(bytes);
  }

  String decodeBase64(String text) {
    var bytes = base64.decode(text);
    return String.fromCharCodes(bytes);
  }
}
