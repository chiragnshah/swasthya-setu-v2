import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:swasthyasetu/app_screens/chat_screen.dart';
import 'package:swasthyasetu/app_screens/doctor_full_details_screen.dart';
import 'package:swasthyasetu/app_screens/patient_doctor_permissions_screen.dart';
import 'package:swasthyasetu/app_screens/video_call_screen.dart';
import 'package:swasthyasetu/controllers/payment_webview_controller.dart';
import 'package:swasthyasetu/global/SizeConfig.dart';
import 'package:swasthyasetu/global/utils.dart';
import 'package:swasthyasetu/podo/response_main_model.dart';

import 'package:http/http.dart' as http;

import '../utils/color.dart';
import '../utils/progress_dialog.dart';
import 'appointment_doctors_list.dart';
import 'fullscreen_image.dart';
import 'not_connected_doctors_list.dart';

class DoctorsListScreen extends StatefulWidget {
  String patientIDP = "";
  final String urlFetchPatientProfileDetails =
      "${baseURL}patientProfileData.php";

  String emptyTextMyDoctors1 =
      "Ask your Doctor to send you bind request from Swasthya setu Doctor panel so that you can able to share all your Health records to your prefered doctor.";

  String emptyMessage = "";

  Widget emptyMessageWidget;

  DoctorsListScreen(String patientIDP) {
    this.patientIDP = patientIDP;
  }

  @override
  State<StatefulWidget> createState() {
    return DoctorsListScreenState();
  }
}

class DoctorsListScreenState extends State<DoctorsListScreen> {
  List<Map<String, String>> listDoctors = [];
  List<Map<String, String>> listDoctorsSearchResults = [];
  String cityIDF = "";
  String firstName = "";
  String lastName = "";

  var searchController = TextEditingController();
  var focusNode = new FocusNode();
  var isFABVisible = true;

  Icon icon = Icon(
    Icons.search,
    color: Colors.white,
  );
  Widget titleWidget = Text("My Doctors");
  ScrollController hideFABController;

  @override
  void initState() {
    super.initState();
    widget.emptyMessage = "${widget.emptyTextMyDoctors1}";
    widget.emptyMessageWidget = SizedBox(
      height: SizeConfig.blockSizeVertical * 80,
      child: Container(
        padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage("images/ic_idea_new.png"),
              width: 100,
              height: 100,
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              "${widget.emptyMessage}",
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ),
    );
    hideFABController = ScrollController();
    hideFABController.addListener(() {
      if (hideFABController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (isFABVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds
             */
          print("**** $isFABVisible up"); //Move IO away from setState
          setState(() {
            isFABVisible = false;
          });
        }
      } else {
        if (hideFABController.position.userScrollDirection ==
            ScrollDirection.forward) {
          if (isFABVisible == false) {
            print("**** ${isFABVisible} down"); //Move IO away from setState
            setState(() {
              isFABVisible = true;
            });
          }
        }
      }
    });
    getPatientProfileDetails();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: titleWidget,
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(
            color: colorBlack, size: SizeConfig.blockSizeVertical * 2.5),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              setState(() {
                if (icon.icon == Icons.search) {
                  searchController = TextEditingController(text: "");
                  focusNode.requestFocus();
                  this.icon = Icon(
                    Icons.cancel,
                    color: Colors.white,
                  );
                  this.titleWidget = TextField(
                    controller: searchController,
                    focusNode: focusNode,
                    cursorColor: Colors.white,
                    onChanged: (text) {
                      setState(() {
                        if (listDoctors.length > 0)
                          listDoctorsSearchResults = listDoctors
                              .where((objDoctor) =>
                                  objDoctor["FirstName"]
                                      .toLowerCase()
                                      .contains(text.toLowerCase()) ||
                                  objDoctor["LastName"]
                                      .toLowerCase()
                                      .contains(text.toLowerCase()) ||
                                  objDoctor["Specility"]
                                      .toLowerCase()
                                      .contains(text.toLowerCase()) ||
                                  objDoctor["CityName"]
                                      .toLowerCase()
                                      .contains(text.toLowerCase()))
                              .toList();
                        else
                          listDoctorsSearchResults = [];
                      });
                    },
                    style: TextStyle(
                      color: colorBlack,
                      fontSize: SizeConfig.blockSizeHorizontal * 4.0,
                      letterSpacing: 1.5,
                    ),
                    decoration: InputDecoration(
                      /*hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize:
                          SizeConfig.blockSizeVertical * 2.1),
                      labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize:
                          SizeConfig.blockSizeVertical * 2.1),*/
                      hintText: "Doc Name, Speciality or City",
                    ),
                  );
                } else {
                  this.icon = Icon(
                    Icons.search,
                    color: Colors.white,
                  );
                  this.titleWidget = Text("My Doctors");
                  listDoctorsSearchResults = listDoctors;
                }
              });
            },
            icon: icon,
          )
        ], toolbarTextStyle: TextTheme(
            headline6: TextStyle(
                color: colorBlack,
                fontFamily: "Ubuntu",
                fontSize: SizeConfig.blockSizeVertical * 2.5)).bodyText2,
        titleTextStyle: TextTheme(
            headline6: TextStyle(
                color: colorBlack,
                fontFamily: "Ubuntu",
                fontSize: SizeConfig.blockSizeVertical * 2.5)).headline6,
      ),
      floatingActionButton: Visibility(
          visible: isFABVisible,
          child:
              /*FloatingActionButton(
            onPressed: () {
              Get.to(() => NotConnectedDoctorsListScreen(widget.patientIDP));
            },
            child:*/
              MaterialButton(
            onPressed: () {
              Get.to(() => NotConnectedDoctorsListScreen(widget.patientIDP));
            },
            color: Colors.black,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
              SizeConfig.blockSizeHorizontal * 20.0,
            )),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.add,
                  color: Colors.white,
                ),
                SizedBox(
                  width: SizeConfig.blockSizeHorizontal * 3.0,
                ),
                Text(
                  "Add new Doctor",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          )
          /*backgroundColor: Colors.black,*/
          /*)*/
          ),
      body: Builder(
        builder: (context) {
          return RefreshIndicator(
            child: listDoctorsSearchResults.length > 0
                ? Column(
                    children: <Widget>[
                      Expanded(
                        child: ListView.builder(
                            itemCount: listDoctorsSearchResults.length,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Get.to(() => DoctorFullDetailsScreen(
                                        listDoctorsSearchResults[index],
                                        widget.patientIDP,
                                      )).then((value) {
                                    if (value != null && value == 1)
                                      getPatientProfileDetails();
                                  });
                                },
                                child: Container(
                                    /*decoration: BoxDecoration(
                                        color: listDoctorsSearchResults[index]
                                                    ["BindedTag"] ==
                                                "1"
                                            ? Colors.white
                                            : Colors.white,
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 1.0,
                                                color: Colors.grey))),*/
                                    child: Padding(
                                        padding: EdgeInsets.all(
                                            SizeConfig.blockSizeHorizontal * 2),
                                        child: Column(
                                          children: [
                                            Row(
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) {
                                                        return FullScreenImage(
                                                          "$doctorImgUrl${listDoctorsSearchResults[index]["DoctorImage"]}",
                                                          heroTag:
                                                              "fullImg_$doctorImgUrl${listDoctorsSearchResults[index]["DoctorImage"]}_${listDoctorsSearchResults[index]['DoctorIDP']}",
                                                          showPlaceholder:
                                                              !isImageNotNullAndBlank(
                                                                  index),
                                                        );
                                                      }));
                                                    },
                                                    child: CircleAvatar(
                                                        radius: SizeConfig
                                                                .blockSizeHorizontal *
                                                            6,
                                                        backgroundImage:
                                                            isImageNotNullAndBlank(
                                                                    index)
                                                                ? NetworkImage(
                                                                    "$doctorImgUrl${listDoctorsSearchResults[index]["DoctorImage"]}")
                                                                : AssetImage(
                                                                    "images/ic_user_placeholder.png")),
                                                  ),
                                                  SizedBox(
                                                    width: SizeConfig
                                                            .blockSizeHorizontal *
                                                        5,
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Container(
                                                        width: SizeConfig
                                                                .blockSizeHorizontal *
                                                            70,
                                                        child: Row(
                                                          children: [
                                                            Expanded(
                                                              child: Text(
                                                                (listDoctorsSearchResults[index]["FirstName"]
                                                                            .trim() +
                                                                        " " +
                                                                        listDoctorsSearchResults[index]["LastName"]
                                                                            .trim())
                                                                    .trim(),
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .blockSizeHorizontal *
                                                                          4.2,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  color: listDoctorsSearchResults[index]
                                                                              [
                                                                              "BindedTag"] ==
                                                                          "1"
                                                                      ? Colors
                                                                          .black
                                                                      : Colors
                                                                          .black,
                                                                  letterSpacing:
                                                                      1.3,
                                                                ),
                                                              ),
                                                            ),
                                                            /*InkWell(
                                                              onTap: () {
                                                                String
                                                                    patientIDP =
                                                                    listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        'DoctorIDP'];
                                                                String patientName = listDoctorsSearchResults[index]
                                                                            [
                                                                            'FirstName']
                                                                        .trim() +
                                                                    " " +
                                                                    listDoctorsSearchResults[index]
                                                                            [
                                                                            'LastName']
                                                                        .trim();
                                                                String
                                                                    doctorImage =
                                                                    "$doctorImgUrl${listDoctorsSearchResults[index]["DoctorImage"]}";
                                                                Navigator.of(
                                                                        context)
                                                                    .push(MaterialPageRoute(
                                                                        builder:
                                                                            (context) {
                                                                  return ChatScreen(
                                                                    patientIDP:
                                                                        patientIDP,
                                                                    patientName:
                                                                        patientName,
                                                                    patientImage:
                                                                        doctorImage,
                                                                    heroTag:
                                                                        "selectedDoctor_$index",
                                                                  );
                                                                }));
                                                              },
                                                              child: Image(
                                                                image:
                                                                    AssetImage(
                                                                  "images/ic_chat.png",
                                                                ),
                                                                color: Colors
                                                                    .blueGrey,
                                                                width: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    5.5,
                                                                height: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    5.5,
                                                              ),
                                                            ),*/
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: SizeConfig
                                                                .blockSizeVertical *
                                                            0.5,
                                                      ),
                                                      Text(
                                                        listDoctorsSearchResults[
                                                                    index]
                                                                ["Specility"] +
                                                            " - " +
                                                            listDoctorsSearchResults[
                                                                    index]
                                                                ["CityName"],
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                          fontSize: SizeConfig
                                                                  .blockSizeHorizontal *
                                                              3.3,
                                                          color: listDoctorsSearchResults[
                                                                          index]
                                                                      [
                                                                      "BindedTag"] ==
                                                                  "1"
                                                              ? Colors.grey
                                                              : Colors.grey,
                                                          letterSpacing: 1.3,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ]),
                                            SizedBox(
                                              height:
                                                  SizeConfig.blockSizeVertical *
                                                      0.5,
                                            ),
                                            Align(
                                                alignment: Alignment.centerLeft,
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                    horizontal: SizeConfig
                                                            .blockSizeHorizontal *
                                                        12,
                                                  ),
                                                  child: Row(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: [
                                                      listDoctorsSearchResults[
                                                                      index][
                                                                  "BindedTag"] ==
                                                              "1"
                                                          ? SizedBox(
                                                              width: SizeConfig
                                                                      .blockSizeHorizontal *
                                                                  5.0,
                                                            )
                                                          : Container(),
                                                      InkWell(
                                                          onTap: () {
                                                            String patientIDP =
                                                                listDoctorsSearchResults[
                                                                        index][
                                                                    'DoctorIDP'];
                                                            String patientName = listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        'FirstName']
                                                                    .trim() +
                                                                " " +
                                                                listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        'LastName']
                                                                    .trim();
                                                            String doctorImage =
                                                                "$doctorImgUrl${listDoctorsSearchResults[index]["DoctorImage"]}";
                                                            Get.to(() =>
                                                                    ChatScreen(
                                                                      patientIDP:
                                                                          patientIDP,
                                                                      patientName:
                                                                          patientName,
                                                                      patientImage:
                                                                          doctorImage,
                                                                      heroTag:
                                                                          "selectedDoctor_${listDoctorsSearchResults[index]['DoctorIDP']}",
                                                                    ))
                                                                .then((value) {
                                                              getPatientProfileDetails();
                                                            });
                                                          },
                                                          child: Row(
                                                            children: [
                                                              Image(
                                                                image: AssetImage(
                                                                    "images/ic_ask_to_doctor_filled.png"),
                                                                width: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    7.5,
                                                                height: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    7.5,
                                                              ),
                                                              SizedBox(
                                                                width: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    1.0,
                                                              ),
                                                              Text(
                                                                "Chat",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        700]),
                                                              )
                                                            ],
                                                          )),
                                                      SizedBox(
                                                        width: SizeConfig
                                                                .blockSizeHorizontal *
                                                            5.0,
                                                      ),
                                                      InkWell(
                                                          onTap: () {
                                                            Get.to(() =>
                                                                DoctorFullDetailsScreen(
                                                                  listDoctorsSearchResults[
                                                                      index],
                                                                  widget
                                                                      .patientIDP,
                                                                )).then((value) {
                                                              if (value !=
                                                                      null &&
                                                                  value == 1)
                                                                getPatientProfileDetails();
                                                            });
                                                            /*startPayment(
                                                                1,
                                                                listDoctorsSearchResults[
                                                                    index]);*/
                                                            /*Get.to(() =>
                                                                PatientDoctorPermissionScreen(
                                                                  listDoctorsSearchResults[
                                                                          index]
                                                                      [
                                                                      "DoctorIDP"],
                                                                  listDoctorsSearchResults[
                                                                          index]
                                                                      [
                                                                      "HealthRecordsDisplayStatus"],
                                                                  listDoctorsSearchResults[
                                                                          index]
                                                                      [
                                                                      "ConsultationDisplayStatus"],
                                                                )).then((value) {
                                                              getPatientProfileDetails();
                                                            });*/
                                                          },
                                                          child: Row(
                                                            children: [
                                                              Image(
                                                                image: AssetImage(
                                                                    "images/ic_doctor_profile.png"),
                                                                width: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    7.0,
                                                                height: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    7.0,
                                                              ),
                                                              SizedBox(
                                                                width: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    1.0,
                                                              ),
                                                              Text(
                                                                "View Profile",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .blue[
                                                                        700]),
                                                              )
                                                            ],
                                                          )),
                                                      /*listDoctorsSearchResults[
                                                                      index][
                                                                  "BindedTag"] ==
                                                              "1"
                                                          ? InkWell(
                                                              onTap: () {
                                                                showConfirmationDialogForUnbind(
                                                                    context,
                                                                    "0",
                                                                    listDoctorsSearchResults[
                                                                        index]);
                                                              },
                                                              child: Row(
                                                                children: [
                                                                  Container(
                                                                    padding:
                                                                        EdgeInsets.all(
                                                                            1.0),
                                                                    decoration: BoxDecoration(
                                                                        shape: BoxShape
                                                                            .circle,
                                                                        color: Colors
                                                                            .red),
                                                                    child: Icon(
                                                                      Icons
                                                                          .remove,
                                                                      size: SizeConfig
                                                                              .blockSizeHorizontal *
                                                                          5.0,
                                                                      color: Colors
                                                                          .white,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    width: SizeConfig
                                                                            .blockSizeHorizontal *
                                                                        1.0,
                                                                  ),
                                                                  Text(
                                                                    "Remove Doctor",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .red),
                                                                  )
                                                                ],
                                                              ))
                                                          : InkWell(
                                                              onTap: () {
                                                                bindUnbindDoctor(
                                                                    "1",
                                                                    listDoctorsSearchResults[
                                                                        index]);
                                                              },
                                                              child: Icon(
                                                                Icons.add,
                                                                size: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    8,
                                                                color: Colors
                                                                    .green,
                                                              ),
                                                            ),*/
                                                    ],
                                                  ),
                                                )),
                                            Container(
                                              color: Colors.grey,
                                              height: 0.5,
                                            )
                                                .paddingSymmetric(
                                                    horizontal: SizeConfig
                                                            .blockSizeHorizontal *
                                                        2)
                                                .paddingOnly(
                                                  top: SizeConfig
                                                          .blockSizeVertical *
                                                      1.8,
                                                )
                                            /*Padding(
                                              padding: EdgeInsets.only(
                                                left: SizeConfig
                                                        .blockSizeHorizontal *
                                                    18,
                                                top: SizeConfig
                                                        .blockSizeVertical *
                                                    0.4,
                                              ),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: InkWell(
                                                      onTap: () {
                                                        String patientIDP =
                                                            listDoctorsSearchResults[
                                                                    index]
                                                                ["DoctorIDP"];
                                                        //"FirstName": jo['FirstName'].toString(),
                                                        //              "LastName": jo['LastName'].toString(),
                                                        String patientName = (listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        "FirstName"]
                                                                    .trim() +
                                                                " " +
                                                                listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        "LastName"]
                                                                    .trim())
                                                            .replaceAll(
                                                                "  ", " ");

                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) {
                                                          return ChatScreen(
                                                              patientIDP:
                                                                  patientIDP,
                                                              patientName:
                                                                  patientName,
                                                              patientImage:
                                                                  listDoctorsSearchResults[
                                                                          index]
                                                                      [
                                                                      "DoctorImage"]);
                                                        }));
                                                      },
                                                      child: Row(
                                                        children: [
                                                          Image(
                                                            image: AssetImage(
                                                              "images/ic_chat.png",
                                                            ),
                                                            color:
                                                                Colors.blueGrey,
                                                            width: SizeConfig
                                                                    .blockSizeHorizontal *
                                                                5.5,
                                                            height: SizeConfig
                                                                    .blockSizeHorizontal *
                                                                5.5,
                                                          ),
                                                          SizedBox(
                                                            width: SizeConfig
                                                                    .blockSizeHorizontal *
                                                                1.0,
                                                          ),
                                                          Text(
                                                            "Chat",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    3.5),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: InkWell(
                                                      onTap: () async {
                                                        String doctorIDP =
                                                            listDoctorsSearchResults[
                                                                    index]
                                                                ["DoctorIDP"];
                                                        String doctorName =
                                                            listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        "FirstName"]
                                                                    .trim() +
                                                                " " +
                                                                listDoctorsSearchResults[
                                                                            index]
                                                                        [
                                                                        "LastName"]
                                                                    .trim();
                                                        String doctorImage =
                                                            listDoctorsSearchResults[
                                                                    index]
                                                                ["DoctorImage"];
                                                        */ /*getChannelIDForVideoCall(
                                                            patientIDP,
                                                            "",
                                                            "0");*/ /*
                                                        showVideoCallRequestDialog(
                                                            context,
                                                            doctorIDP,
                                                            doctorName,
                                                            doctorImage);
                                                        */ /*requestVideoCall(
                                                            doctorIDP,
                                                            doctorName,
                                                            doctorImage);*/ /*
                                                      },
                                                      child: Row(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Icon(
                                                            Icons.video_call,
                                                            size: SizeConfig
                                                                    .blockSizeHorizontal *
                                                                6.5,
                                                            color:
                                                                Colors.blueGrey,
                                                          ),
                                                          */ /*Image(
                                                                    image:
                                                                    AssetImage(
                                                                      "images/ic_chat.png",
                                                                    ),
                                                                    color: Colors
                                                                        .blueGrey,
                                                                    width: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                        5.5,
                                                                    height: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                        5.5,
                                                                  ),*/ /*
                                                          SizedBox(
                                                            width: SizeConfig
                                                                    .blockSizeHorizontal *
                                                                1.0,
                                                          ),
                                                          Text(
                                                            "Video Call",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: SizeConfig
                                                                        .blockSizeHorizontal *
                                                                    3.5),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),*/
                                          ],
                                        ))),
                              );
                            }),
                      ),
                    ],
                  )
                : widget.emptyMessageWidget,
            onRefresh: () {
              return getPatientProfileDetails();
            },
          );
        },
      ),
    );
  }

  isImageNotNullAndBlank(int index) {
    return (listDoctorsSearchResults[index]["DoctorImage"] != "" &&
        listDoctorsSearchResults[index]["DoctorImage"] != null &&
        listDoctorsSearchResults[index]["DoctorImage"] != "null");
  }

  void getChannelIDForVideoCall(
      String doctorIDP, String patientName, String startOrEnd) async {
    final String urlGetChannelIDForVidCall = "${baseURL}videocall.php";
    ProgressDialog pr = ProgressDialog(context);
    pr.show();

    try {
      String patientUniqueKey = await getPatientUniqueKey();
      String userType = await getUserType();
      String patientIDP = await getPatientOrDoctorIDP();
      debugPrint("Key and type");
      debugPrint(patientUniqueKey);
      debugPrint(userType);
      String fromType = "";
      if (userType == "patient") {
        fromType = "P";
      } else if (userType == "doctor") {
        fromType = "D";
      }
      String startOrEndCall = "";
      if (startOrEnd == "0") {
        startOrEndCall = "startcall";
      } else if (startOrEnd == "1") {
        startOrEndCall = "endcall";
      }
      String jsonStr = "{" +
          "\"PatientIDP\":\"$patientIDP\"" +
          ",\"DoctorIDP\":\"$doctorIDP\"" +
          ",\"FromType\":\"$fromType\"" +
          ",\"CallType\":\"$startOrEndCall\"" +
          /*"PatientIDP" +
          "\"" +
          ":" +
          "\"" +
          patientIDP +
          "\"" +*/
          "}";

      debugPrint(jsonStr);

      String encodedJSONStr = encodeBase64(jsonStr);
      var response = await apiHelper.callApiWithHeadersAndBody(
        url: urlGetChannelIDForVidCall,
        //Uri.parse(loginUrl),
        headers: {
          "u": patientUniqueKey,
          "type": userType,
        },
        body: {"getjson": encodedJSONStr},
      );
      debugPrint(response.body.toString());
      final jsonResponse = json.decode(response.body.toString());
      ResponseModel model = ResponseModel.fromJSON(jsonResponse);
      pr.hide();
      if (model.status == "OK") {
        var data = jsonResponse['Data'];
        var strData = decodeBase64(data);
        debugPrint("Decoded Data Array : " + strData);
        final jsonData = json.decode(strData);
        //debugPrint("Got height value - ${jsonData[0]['Height']}");
        await _handlePermission(Permission.camera);
        await _handlePermission(Permission.microphone);
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return VideoCallScreen(
            patientIDP: doctorIDP,
            channelID: jsonData[0]['VideoID'].toString(),
          );
        }));
        setState(() {});
      } else {}
    } catch (exception) {}
  }

  void requestVideoCall(
      String doctorIDP, String doctorName, String image) async {
    final String urlGetChannelIDForVidCall = "${baseURL}videocallRequest.php";
    ProgressDialog pr = ProgressDialog(context);
    pr.show();

    try {
      String patientUniqueKey = await getPatientUniqueKey();
      String userType = await getUserType();
      String patientIDP = await getPatientOrDoctorIDP();
      debugPrint("Key and type");
      debugPrint(patientUniqueKey);
      debugPrint(userType);
      String fromType = "";
      if (userType == "patient") {
        fromType = "P";
      } else if (userType == "doctor") {
        fromType = "D";
      }
      String jsonStr = "{" +
          "\"PatientIDP\":\"$patientIDP\"" +
          ",\"DoctorIDP\":\"$doctorIDP\"" +
          ",\"messagefrom\":\"$fromType\"" +
          "}";

      debugPrint(jsonStr);

      String encodedJSONStr = encodeBase64(jsonStr);
      var response = await apiHelper.callApiWithHeadersAndBody(
        url: urlGetChannelIDForVidCall,
        //Uri.parse(loginUrl),
        headers: {
          "u": patientUniqueKey,
          "type": userType,
        },
        body: {"getjson": encodedJSONStr},
      );
      debugPrint(response.body.toString());
      final jsonResponse = json.decode(response.body.toString());
      ResponseModel model = ResponseModel.fromJSON(jsonResponse);
      pr.hide();
      if (model.status == "OK") {
        var data = jsonResponse['Data'];
        var strData = decodeBase64(data);
        debugPrint("Decoded Data Array : " + strData);
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) {
            return ChatScreen(
              patientIDP: doctorIDP,
              patientName: doctorName,
              patientImage: image,
            );
          },
        ));
        setState(() {});
      } else {}
    } catch (exception) {}
  }

  Future<void> _handlePermission(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  showConfirmationDialogForUnbind(
      BuildContext context, String bindFlag, Map<String, String> doctorData) {
    var title = "Are you sure to remove this doctor?";
    var subTitle =
        "By doing this the selected doctor will be removed from your list and you need to bind this doctor again.";
    showDialog(
        context: context,
        barrierDismissible: false,
        // user must tap button for close dialog!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(subTitle),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("No")),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    bindUnbindDoctor(bindFlag, doctorData);
                  },
                  child: Text("Yes"))
            ],
          );
        });
  }

  String encodeBase64(String text) {
    var bytes = utf8.encode(text);
    //var base64str =
    return base64.encode(bytes);
    //= Base64Encoder().convert()
  }

  String decodeBase64(String text) {
    //var bytes = utf8.encode(text);
    //var base64str =
    var bytes = base64.decode(text);
    return String.fromCharCodes(bytes);
    //= Base64Encoder().convert()
  }

  Future<String> getPatientProfileDetails() async {
    /*List<IconModel> listIcon;*/
    var doctorApiCalled = false;
    ProgressDialog pr = ProgressDialog(context);
    Future.delayed(Duration.zero, () {
      pr.show();
    });

    try {
      String patientUniqueKey = await getPatientUniqueKey();
      String userType = await getUserType();
      debugPrint("Key and type");
      debugPrint(patientUniqueKey);
      debugPrint(userType);
      String jsonStr = "{" +
          "\"" +
          "PatientIDP" +
          "\"" +
          ":" +
          "\"" +
          widget.patientIDP +
          "\"" +
          "}";

      debugPrint(jsonStr);

      String encodedJSONStr = encodeBase64(jsonStr);
      //listIcon = new List();
      var response = await apiHelper.callApiWithHeadersAndBody(
        url: widget.urlFetchPatientProfileDetails,
        headers: {
          "u": patientUniqueKey,
          "type": userType,
        },
        body: {"getjson": encodedJSONStr},
      );
      //var resBody = json.decode(response.body);
      debugPrint(response.body.toString());
      final jsonResponse = json.decode(response.body.toString());
      ResponseModel model = ResponseModel.fromJSON(jsonResponse);
      if (model.status == "OK") {
        var data = jsonResponse['Data'];
        var strData = decodeBase64(data);
        debugPrint("Decoded Data Array : " + strData);
        final jsonData = json.decode(strData);
        cityIDF = jsonData[0]['CityIDF'];
        firstName = jsonData[0]['FirstName'];
        lastName = jsonData[0]['LastName'];

        listDoctors = [];
        listDoctorsSearchResults = [];
        String loginUrl = "${baseURL}doctorList.php";
        //listIcon = new List();
        String patientUniqueKey = await getPatientUniqueKey();
        String userType = await getUserType();
        debugPrint("Key and type");
        debugPrint(patientUniqueKey);
        debugPrint(userType);
        var cityIdp = "";
        if (cityIDF != "")
          cityIdp = cityIDF;
        else
          cityIdp = "-";
        String jsonStr = "{" +
            "\"" +
            "PatientIDP" +
            "\"" +
            ":" +
            "\"" +
            widget.patientIDP +
            "\"" +
            "," +
            "\"" +
            "CityIDP" +
            "\"" +
            ":" +
            "\"" +
            cityIdp +
            "\"" +
            "}";

        debugPrint("Doctor API request object");
        debugPrint(jsonStr);

        String encodedJSONStr = encodeBase64(jsonStr);
        var response = await apiHelper.callApiWithHeadersAndBody(
          url: loginUrl,
          //Uri.parse(loginUrl),
          headers: {
            "u": patientUniqueKey,
            "type": userType,
          },
          body: {"getjson": encodedJSONStr},
        );
        //var resBody = json.decode(response.body);
        debugPrint(response.body.toString());
        final jsonResponse1 = json.decode(response.body.toString());
        ResponseModel model = ResponseModel.fromJSON(jsonResponse1);
        pr.hide();

        if (model.status == "OK") {
          var data = jsonResponse1['Data'];
          var strData = decodeBase64(data);
          debugPrint("Decoded Data Array Dashboard : " + strData);
          final jsonData = json.decode(strData);
          if (jsonData.length > 0)
            doctorApiCalled = true;
          else
            doctorApiCalled = false;
          for (var i = 0; i < jsonData.length; i++) {
            var jo = jsonData[i];
            listDoctors.add({
              "DoctorIDP": jo['DoctorIDP'].toString(),
              "DoctorID": jo['DoctorID'].toString(),
              "FirstName": jo['FirstName'].toString(),
              "LastName": jo['LastName'].toString(),
              "MobileNo": jo['MobileNo'].toString(),
              "Specility": jo['Specility'].toString(),
              "DoctorImage": jo['DoctorImage'].toString(),
              "CityName": jo['CityName'].toString(),
              "BindedTag": jo['BindedTag'].toString(),
              "HealthRecordsDisplayStatus":
                  jo['HealthRecordsDisplayStatus'].toString(),
              "ConsultationDisplayStatus":
                  jo['ConsultationDisplayStatus'].toString(),
              "DueAmount": jo['DueAmount'].toString(),
            });
            listDoctorsSearchResults.add({
              "DoctorIDP": jo['DoctorIDP'].toString(),
              "DoctorID": jo['DoctorID'].toString(),
              "FirstName": jo['FirstName'].toString(),
              "LastName": jo['LastName'].toString(),
              "MobileNo": jo['MobileNo'].toString(),
              "Specility": jo['Specility'].toString(),
              "DoctorImage": jo['DoctorImage'].toString(),
              "CityName": jo['CityName'].toString(),
              "BindedTag": jo['BindedTag'].toString(),
              "HealthRecordsDisplayStatus":
                  jo['HealthRecordsDisplayStatus'].toString(),
              "ConsultationDisplayStatus":
                  jo['ConsultationDisplayStatus'].toString(),
              "DueAmount": jo['DueAmount'].toString(),
            });
          }
          setState(() {});
        }
        //setState(() {});
      } else {
        final snackBar = SnackBar(
          backgroundColor: Colors.red,
          content: Text(model.message),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } catch (exception) {
      if (!doctorApiCalled) {
        try {
          listDoctors = [];
          listDoctorsSearchResults = [];
          String loginUrl = "${baseURL}doctorList.php";
          //listIcon = new List();
          String patientUniqueKey = await getPatientUniqueKey();
          String userType = await getUserType();
          debugPrint("Key and type");
          debugPrint(patientUniqueKey);
          debugPrint(userType);
          var cityIdp = "";
          if (cityIDF != "")
            cityIdp = cityIDF;
          else
            cityIdp = "-";
          String jsonStr = "{" +
              "\"" +
              "PatientIDP" +
              "\"" +
              ":" +
              "\"" +
              widget.patientIDP +
              "\"" +
              "," +
              "\"" +
              "CityIDP" +
              "\"" +
              ":" +
              "\"" +
              cityIdp +
              "\"" +
              "}";

          debugPrint("Doctor API request object");
          debugPrint(jsonStr);

          String encodedJSONStr = encodeBase64(jsonStr);
          var response = await apiHelper.callApiWithHeadersAndBody(
            url: loginUrl,
            //Uri.parse(loginUrl),
            headers: {
              "u": patientUniqueKey,
              "type": userType,
            },
            body: {"getjson": encodedJSONStr},
          );
          //var resBody = json.decode(response.body);
          debugPrint(response.body.toString());
          final jsonResponse1 = json.decode(response.body.toString());
          ResponseModel model = ResponseModel.fromJSON(jsonResponse1);
          pr.hide();

          if (model.status == "OK") {
            var data = jsonResponse1['Data'];
            var strData = decodeBase64(data);
            debugPrint("Decoded Data Array Dashboard : " + strData);
            final jsonData = json.decode(strData);
            if (jsonData.length > 0)
              doctorApiCalled = true;
            else
              doctorApiCalled = false;
            for (var i = 0; i < jsonData.length; i++) {
              var jo = jsonData[i];
              listDoctors.add({
                "DoctorIDP": jo['DoctorIDP'].toString(),
                "DoctorID": jo['DoctorID'].toString(),
                "FirstName": jo['FirstName'].toString(),
                "LastName": jo['LastName'].toString(),
                "MobileNo": jo['MobileNo'].toString(),
                "Specility": jo['Specility'].toString(),
                "DoctorImage": jo['DoctorImage'].toString(),
                "CityName": jo['CityName'].toString(),
                "BindedTag": jo['BindedTag'].toString(),
                "HealthRecordsDisplayStatus":
                    jo['HealthRecordsDisplayStatus'].toString(),
                "ConsultationDisplayStatus":
                    jo['ConsultationDisplayStatus'].toString(),
                "DueAmount": jo['DueAmount'].toString(),
              });
              listDoctorsSearchResults.add({
                "DoctorIDP": jo['DoctorIDP'].toString(),
                "DoctorID": jo['DoctorID'].toString(),
                "FirstName": jo['FirstName'].toString(),
                "LastName": jo['LastName'].toString(),
                "MobileNo": jo['MobileNo'].toString(),
                "Specility": jo['Specility'].toString(),
                "DoctorImage": jo['DoctorImage'].toString(),
                "CityName": jo['CityName'].toString(),
                "BindedTag": jo['BindedTag'].toString(),
                "HealthRecordsDisplayStatus":
                    jo['HealthRecordsDisplayStatus'].toString(),
                "ConsultationDisplayStatus":
                    jo['ConsultationDisplayStatus'].toString(),
                "DueAmount": jo['DueAmount'].toString(),
              });
            }
            setState(() {});
          }
        } catch (exception) {
          pr.hide();
        }
      } else {
        pr.hide();
      }
    }

    return 'success';
  }

  Future<String> getDoctorsList() async {
    //to call city api first to get the city idp of the patient
    String loginUrl = "${baseURL}doctorList.php";
    ProgressDialog pr;
    Future.delayed(Duration.zero, () {
      pr = ProgressDialog(context);
      pr.show();
    });
    //listIcon = new List();
    String patientUniqueKey = await getPatientUniqueKey();
    String userType = await getUserType();
    debugPrint("Key and type");
    debugPrint(patientUniqueKey);
    debugPrint(userType);
    String jsonStr = "{" +
        "\"" +
        "PatientIDP" +
        "\"" +
        ":" +
        "\"" +
        widget.patientIDP +
        "\"" +
        "," +
        "\"" +
        "CityIDP" +
        "\"" +
        ":" +
        "\"" +
        cityIDF +
        "\"" +
        "}";

    debugPrint(jsonStr);

    String encodedJSONStr = encodeBase64(jsonStr);
    var response = await apiHelper.callApiWithHeadersAndBody(
      url: loginUrl,
      //Uri.parse(loginUrl),
      headers: {
        "u": patientUniqueKey,
        "type": userType,
      },
      body: {"getjson": encodedJSONStr},
    );
    //var resBody = json.decode(response.body);
    debugPrint(response.body.toString());
    final jsonResponse = json.decode(response.body.toString());
    ResponseModel model = ResponseModel.fromJSON(jsonResponse);
    pr.hide();

    if (model.status == "OK") {
      var data = jsonResponse['Data'];
      var strData = decodeBase64(data);
      debugPrint("Decoded Data Array Dashboard : " + strData);
      final jsonData = json.decode(strData);
      for (var i = 0; i < jsonData.length; i++) {
        var jo = jsonData[i];
        /*listVital.add(ModelGraphValues(
            jo['VitalEntryDate'], jo['VitalEntryTime'], jo['VitalValue']));*/
      }
      setState(() {});
      return 'success';
    }
  }

  void bindUnbindDoctor(String bindFlag, Map<String, String> doctorData) async {
    String loginUrl = "${baseURL}patientBindDoctor.php";
    ProgressDialog pr;
    Future.delayed(Duration.zero, () {
      pr = ProgressDialog(context);
      pr.show();
    });
    //listIcon = new List();
    String patientUniqueKey = await getPatientUniqueKey();
    String userType = await getUserType();
    debugPrint("Key and type");
    debugPrint(patientUniqueKey);
    debugPrint(userType);
    String jsonStr = "{" +
        "\"" +
        "PatientIDP" +
        "\"" +
        ":" +
        "\"" +
        widget.patientIDP +
        "\"" +
        "," +
        "\"" +
        "DoctorIDP" +
        "\"" +
        ":" +
        "\"" +
        doctorData["DoctorIDP"] +
        "\"" +
        "," +
        "\"" +
        "FirstName" +
        "\"" +
        ":" +
        "\"" +
        firstName +
        "\"" +
        "," +
        "\"" +
        "LastName" +
        "\"" +
        ":" +
        "\"" +
        lastName +
        "\"" +
        "," +
        "\"" +
        "BindFlag" +
        "\"" +
        ":" +
        "\"" +
        bindFlag +
        "\"" +
        "}";

    debugPrint(jsonStr);

    String encodedJSONStr = encodeBase64(jsonStr);
    var response = await apiHelper.callApiWithHeadersAndBody(
      url: loginUrl,
      //Uri.parse(loginUrl),
      headers: {
        "u": patientUniqueKey,
        "type": userType,
      },
      body: {"getjson": encodedJSONStr},
    );
    //var resBody = json.decode(response.body);
    debugPrint(response.body.toString());
    final jsonResponse = json.decode(response.body.toString());
    ResponseModel model = ResponseModel.fromJSON(jsonResponse);
    pr.hide();

    if (model.status == "OK") {
      getPatientProfileDetails();
    }
  }

  void showVideoCallRequestDialog(BuildContext context, String doctorIDP,
      String doctorName, String doctorImage) {
    var title = "Do you want to send Video call request?";
    showDialog(
        context: context,
        barrierDismissible: false,
        // user must tap button for close dialog!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    "No",
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  )),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    requestVideoCall(doctorIDP, doctorName, doctorImage);
                  },
                  child: Text("Yes"))
            ],
          );
        });
  }

  void startPayment(int type, Map<String, String> doctorData) async {
    String patientIDP = await getPatientOrDoctorIDP();
    if (type == 0) {
      goToWebview(context, "",
          "${baseURL}paymentgatewayPayDuetoDoctor.php?appointid=-&amount=${doctorData["DueAmount"]}&idp=${doctorData["DoctorIDP"]}&idppt=$patientIDP");
    } else if (type == 1) {
      goToWebview(context, "",
          "${baseURL}paymentgatewayPaytoDoctorDirect.php?idp=${doctorData["DoctorIDP"]}&idppt=$patientIDP");
    }
  }

  goToWebview(BuildContext context, String iconName, String webView) {
    PaymentWebViewController paymentWebViewController =
        Get.put(PaymentWebViewController());
    String webViewUrl = Uri.encodeFull(webView);
    paymentWebViewController.url.value = webViewUrl;
    debugPrint("encoded url :- $webViewUrl");

    if (webView != "") {
      final flutterWebViewPlugin = FlutterWebviewPlugin();
      flutterWebViewPlugin.onUrlChanged.listen((url) {
        debugPrint(url);
        paymentWebViewController.url.value = url;
      });
      flutterWebViewPlugin.onDestroy.listen((_) {
        if (Navigator.canPop(context)) {
          Navigator.of(context).pop();
        }
      });
      Navigator.push(context, MaterialPageRoute(builder: (mContext) {
        return new MaterialApp(
          debugShowCheckedModeBanner: false,
          theme:
              ThemeData(fontFamily: "Ubuntu", primaryColor: Color(0xFF06A759)),
          routes: {
            "/": (_) => Obx(() => WebviewScaffold(
                  withLocalStorage: true,
                  withJavascript: true,
                  url: webViewUrl,
                  appBar:
                      paymentWebViewController.url.value.contains(
                                  "swasthyasetu.com/ws/failurePayDuetoDoctor.php") ||
                              paymentWebViewController.url.value.contains(
                                  "swasthyasetu.com/ws/successPayDuetoDoctor.php") ||
                              paymentWebViewController.url.value.contains(
                                  "swasthyasetu.com/ws/paymentgatewayPaytoDoctorDirect.php")
                          ? AppBar(
                              backgroundColor: Colors.white,
                              title: Text(iconName),
                              leading: IconButton(
                                  icon: Icon(
                                    Platform.isAndroid
                                        ? Icons.arrow_back
                                        : Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: SizeConfig.blockSizeHorizontal * 8.0,
                                  ),
                                  onPressed: () {
                                    if (paymentWebViewController.url.value.contains(
                                        "swasthyasetu.com/ws/failurePayDuetoDoctor.php"))
                                      Navigator.of(context).pop();
                                    else if (paymentWebViewController.url.value
                                        .contains(
                                            "swasthyasetu.com/ws/successPayDuetoDoctor.php")) {
                                      Navigator.of(context).pop();
                                      //Navigator.of(context).pop();
                                    } else if (paymentWebViewController
                                        .url.value
                                        .contains(
                                            "swasthyasetu.com/ws/paymentgatewayPaytoDoctorDirect.php")) {
                                      Navigator.of(context).pop();
                                      //Navigator.of(context).pop();
                                    }
                                    /*Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CheckExpiryBlankScreen(
                                              "", "coupon", false, null)),
                                  (Route<dynamic> route) => false);*/
                                  }),
                              iconTheme: IconThemeData(
                                  color: Colors.white,
                                  size: SizeConfig.blockSizeVertical * 2.2),
                              textTheme: TextTheme(
                                  subtitle1: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "Ubuntu",
                                      fontSize:
                                          SizeConfig.blockSizeVertical * 2.3)),
                            )
                          : PreferredSize(
                              child: Container(),
                              preferredSize: Size(SizeConfig.screenWidth, 0),
                            ),
                )),
          },
        );
      }));
    }
  }
}
