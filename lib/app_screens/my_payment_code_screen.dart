import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:swasthyasetu/app_screens/doctor_dashboard_screen.dart';
import 'package:swasthyasetu/app_screens/help_screen.dart';
import 'package:swasthyasetu/global/SizeConfig.dart';
import 'package:swasthyasetu/global/utils.dart';
import 'package:swasthyasetu/podo/response_main_model.dart';
import 'package:swasthyasetu/utils/progress_dialog.dart';

import '../utils/color.dart';

class MyPaymentCodeScreen extends StatefulWidget {
  String payGatewayURL;

  MyPaymentCodeScreen(this.payGatewayURL);

  @override
  State<StatefulWidget> createState() {
    return MyPaymentCodeScreenState();
  }
}

class MyPaymentCodeScreenState extends State<MyPaymentCodeScreen> {
  String imgUrl;

  @override
  void initState() {
    super.initState();
    getQRCodeForDoctor();
    /*widget.imgUrl = "";*/
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    getQRCodeForDoctor();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment Code"),
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(color: colorBlack),
        textTheme: TextTheme(
            subtitle1: TextStyle(
          color: colorBlack,
          fontFamily: "Ubuntu",
          fontSize: SizeConfig.blockSizeVertical * 2.5,
        )),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Visibility(
            visible: imgUrl != null && imgUrl != "",
            child: Align(
              alignment: Alignment.center,
              child: imgUrl != null && imgUrl != ""
                  ? Image.network(
                      imgUrl,
                      width: SizeConfig.blockSizeHorizontal * 80,
                      height: SizeConfig.blockSizeHorizontal * 80,
                      fit: BoxFit.cover,
                    )
                  : Container(),
            ),
          ),
          Visibility(
            visible: imgUrl == "",
            child: Container(
              padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage("images/ic_idea_new.png"),
                    width: 100,
                    height: 100,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    "Payment receiving service is not active for your profile.\n\n",
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "To receive payment from patients,",
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "kindly",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: SizeConfig.blockSizeHorizontal * 2.0,
                      ),
                      MaterialButton(
                        onPressed: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return HelpScreen(patientIDP);
                          })).then((value) {
                            getQRCodeForDoctor();
                          });
                        },
                        child: Text(
                          "Contact Us",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal * 4.0,
                          ),
                        ),
                        color: Color(0xFF06A759),
                      ),
                    ],
                  )
                ],
              ),
            ),
            /*Align(
              alignment: Alignment.center,
              child: Text(
                "Payment receiving service is not active for your profile.\n\nTo receive payment from patients, kindly contact us.",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: SizeConfig.blockSizeHorizontal * 5.3,
                    fontWeight: FontWeight.w500),
              ),
            ),*/
          ),
        ],
      ),
    );
  }

  void getQRCodeForDoctor() async {
    String loginUrl = "${baseURL}doctorPayQR.php";
    ProgressDialog pr;
    Future.delayed(Duration.zero, () {
      pr = ProgressDialog(context);
      pr.show();
    });
    //listIcon = new List();
    String patientUniqueKey = await getPatientUniqueKey();
    String userType = await getUserType();
    String patientIDP = await getPatientOrDoctorIDP();
    debugPrint("Key and type");
    debugPrint(patientUniqueKey);
    debugPrint(userType);
    String jsonStr = "{" +
        "\"" +
        "DoctorIDP" +
        "\"" +
        ":" +
        "\"" +
        patientIDP +
        "\"" +
        "," +
        "\"" +
        "PayGatewayURL" +
        "\"" +
        ":" +
        "\"" +
        widget.payGatewayURL +
        "\"" +
        "}";

    debugPrint(jsonStr);

    String encodedJSONStr = encodeBase64(jsonStr);
    var response = await apiHelper.callApiWithHeadersAndBody(
      url: loginUrl,
      headers: {
        "u": patientUniqueKey,
        "type": userType,
      },
      body: {"getjson": encodedJSONStr},
    );
    //var resBody = json.decode(response.body);
    debugPrint(response.body.toString());
    final jsonResponse = json.decode(response.body.toString());
    ResponseModel model = ResponseModel.fromJSON(jsonResponse);
    pr.hide();
    if (model.status == "OK") {
      /*var data = jsonResponse['Data'];
      var strData = decodeBase64(data);
      final jsonData = json.decode(strData);*/
      imgUrl = "${baseURL}images/doctorPayQR/$patientIDP.png";
      debugPrint(imgUrl);
      setState(() {});
    } else {
      imgUrl = "";
      setState(() {});
    }
  }

  String encodeBase64(String text) {
    var bytes = utf8.encode(text);
    //var base64str =
    return base64.encode(bytes);
    //= Base64Encoder().convert()
  }

  String decodeBase64(String text) {
    //var bytes = utf8.encode(text);
    //var base64str =
    var bytes = base64.decode(text);
    return String.fromCharCodes(bytes);
    //= Base64Encoder().convert()
  }
}
