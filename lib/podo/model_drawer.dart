import 'package:flutter/material.dart';

class DrawerModel {
  String title;
  String image;

  DrawerModel({
    this.title,
    this.image,
  });
}
